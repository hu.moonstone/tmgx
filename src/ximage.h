#ifndef TMGX_XIMAGE_H
#define TMGX_XIMAGE_H

#include <X11/Xlib.h>

#define  CBN 64 /* XColor のキャッシュの個数 */

typedef struct _eggx_color_rbuf {
  int cln;
  int clw;
  int clr;
  int clf;
  int red_depth;
  int green_depth;
  int blue_depth;
  int red_sft;
  int green_sft;
  int blue_sft;
  unsigned long msk;
  unsigned long msk_red;
  unsigned long msk_green;
  unsigned long msk_blue;
  XColor cl[CBN];
} eggx_color_rbuf;


extern void init_color_rbuf( eggx_color_rbuf *cache,
                             Display *dis, Colormap cmap,
                             int red_depth, int green_depth, int blue_depth,
                             int red_sft, int green_sft, int blue_sft );

extern int get_idx_from_color_rbuf( eggx_color_rbuf *cache,
                                    Display *dis, Colormap cmap,
                                    unsigned long px );

extern XImage *get_ximage( Display *dis, Colormap cmap,
                           int red_depth, int green_depth, int blue_depth,
                           int red_sft, int green_sft, int blue_sft,
                           Pixmap pix_id, int sx0, int sy0, int width, int height,
                           eggx_color_rbuf *cache );

extern void ximage_to_ppmline( Display *dis, Colormap cmap,
                               eggx_color_rbuf *cache,
                               const XImage *image, int width, int height,
                               int y_idx, int depth, unsigned char *line_buf );

#endif /* TMGX_XIMAGE_H */
